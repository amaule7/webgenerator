module DynMachine
  module PageRenderer
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::FormTagHelper
    include ActionView::Helpers::AssetTagHelper
    include ActionView::Context
    include ApplicationHelper

    def dyn_translate(element)
      e = element
      nested_content = []
      p "#{e.class.name} ID: #{e.id}"
      case e.dyn_tag
        when "content_tag"
          content_tag(e.dyn_type, class: e.dyn_class, id: e.dyn_id, style: e.dyn_style) do
            # create field for ul/li items? There must be a multiple: true setting
            nested_content << e.dyn_content
            nested_content << e.dyn_elements.collect{|element| dyn_translate(element)}.join if e.dyn_elements.any?
            nested_content.join.html_safe
          end
        when "image_tag"
          image_tag(e.dyn_src, alt: e.dyn_alt, class: e.dyn_class, id: e.dyn_id, style: e.dyn_style)
          #Having to put link as asset/image right now prepending image to front
        when "link_tag"
          link_to(e.dyn_href, class: e.dyn_class, id: e.dyn_id, style: e.dyn_style, target: e.dyn_target) do
            nested_content << e.dyn_content
            nested_content << e.dyn_elements.collect{|element| dyn_translate(element)}.join if e.dyn_elements.any?
            nested_content.join.html_safe
          end
        else
          "impossible"
      end
    end

  end
end
