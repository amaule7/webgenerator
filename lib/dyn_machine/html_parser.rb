module DynMachine
  class HtmlParser

    # def initialize()
    # end

    def start_parse
      doc = Nokogiri::HTML.fragment('
          <section class="cbp-so-section dyn-section" id="test">
            <article class="cbp-so-side cbp-so-side-left">
              <a href="http://www.w3schools.com" target="_blank">Visit W3Schools</a>
              <h2>Lemon drops</h2>
              <p>Fruitcake toffee jujubes. Topping biscuit sesame snaps jelly caramels jujubes tiramisu fruitcake. Marzipan tart lemon drops chocolate sesame snaps jelly beans.</p>
            </article>
            <figure class="cbp-so-side cbp-so-side-right">
              <img src="images/1.png" alt="img01">
            </figure>
            <select class="cars" id="select_stuff" style="color:blue;margin-left:30px;">
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
              <option value="mercedes">Mercedes</option>
              <option value="audi">Audi</option>
            </select>
          </section>
        ')

      section_array = doc.css(".dyn-section")
      section_array.each{ |section| parse_element(section) }

    end

    def parse_element(element)
      record_attributes = {}

      record_attributes[:dyn_tag]     = determine_tag(element.name) #e.name is html tag type from nokogiri
      record_attributes[:dyn_type]    = determine_type(element.name)
      record_attributes[:dyn_class]   = element.attributes["class"].value  if element.attributes["class"]
      record_attributes[:dyn_id]      = element.attributes["id"].value     if element.attributes["id"]
      record_attributes[:dyn_style]   = element.attributes["style"].value  if element.attributes["style"]
      record_attributes[:dyn_src]     = element.attributes["src"].value    if element.attributes["src"]
      record_attributes[:dyn_alt]     = element.attributes["alt"].value    if element.attributes["alt"]
      record_attributes[:dyn_value]   = element.attributes["value"].value  if element.attributes["value"]
      record_attributes[:dyn_href]    = element.attributes["href"].value   if element.attributes["href"]
      record_attributes[:dyn_target]  = element.attributes["target"].value if element.attributes["target"]
      record_attributes[:dyn_content] = merge_text_nodes(element)

      record = create_record(record_attributes) #Determine the class + create record

      #Recursive call on children that are elements, if any exist,  link child elements to parent element
      record.dyn_elements << element.element_children.map{ |child| parse_element(child) } if element.element_children.any?
      return record # return record so that children records can be put into collection then pushed into parent object
    end

    def merge_text_nodes(element)
      merged_text_node = element.children.collect do |child|
        #check if child node is text, then check if node contains junk data
        if child.text? && !junk_data(child)
          child.text
        end
      end.join
      merged_text_node.empty? ? nil : merged_text_node
    end

     # (\\n)|(\\t)|( +)
    def junk_data(text_node) #returns true or false
      #checks to see if text node contains junk data
      node_check = text_node.text
      blank = ""
      replace = {"\n" => blank,  "\t" => blank, " " => blank }
      replace.each{ |k, v|  node_check = node_check.gsub(k, v)}
      node_check.empty?
    end

    def create_record(record_attributes)
      dyn_class = case
        when record_attributes[:dyn_class] && record_attributes[:dyn_class].include?("dyn-page")
          DynMachine::DynPage
        when record_attributes[:dyn_class] && record_attributes[:dyn_class].include?("dyn-section")
          DynMachine::DynSection
        else
          DynMachine::DynElement
        end
      dyn_class.create(dyn_class.new.attributes.merge(record_attributes))
    end

    def determine_tag(element_name)
      case element_name
        when "a"   then "link_tag"
        when "img" then "image_tag"
        else "content_tag"
      end
    end

    def determine_type(element_name)
      case element_name
        when "a"    then nil
        when "img"  then nil
        else element_name
      end
    end

  end
  h = HtmlParser.new
  h.start_parse

end
