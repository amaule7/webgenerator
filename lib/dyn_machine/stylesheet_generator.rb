module DynMachine
  class StylesheetGenerator
    attr_reader :section, :section_css, :section_header

    def initialize(section)
      @section        = section
      @section_css    = section.dyn_stylesheet
      @section_header = section_css.split.first
    end

    def make_stylesheet
      stylesheet = []
      stylesheet << add_css_comment(section) + section_css
      stylesheet << build_child_css(section) if section.dyn_elements
      stylesheet.join.html_safe
      # nested_content << "<link href=/dyn_machine/dyn_stylesheets/#{e.id}.css rel=stylesheet type=text/css />"
    end

    private

    def build_child_css(element)
      css_array = []
      element.dyn_elements.each do |child_element|

        #Checks to see if the child object has a children, if so runs recursive loop till all children info returned
        css_array << build_child_css(child_element) if child_element.dyn_elements.any?

        css_array << css_line(child_element)        if child_element.dyn_stylesheet

      end
      css_array.join
    end

    def css_line(element)
      add_css_comment(element) + section_header + " " + element.dyn_stylesheet
    end

    def add_css_comment(element)
      "\n/* #{element.class.name.split('::').last}: #{element.id}, Section-ID: #{@section.id} */\n"
    end

  end
end