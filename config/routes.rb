DynMachine::Engine.routes.draw do
  resources :dyn_pages
  resources :dyn_stylesheets

  root to: "dyn_pages#index"
  get "dyn_pages/build_from_tags" => "dyn_pages#build_from_tags", as: :tags
end