# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150107001604) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dyn_machine_dyn_elements", force: true do |t|
    t.string   "dyn_tag"
    t.string   "dyn_class"
    t.string   "dyn_type"
    t.string   "dyn_id"
    t.string   "dyn_style"
    t.integer  "parent_id"
    t.string   "parent_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dyn_content"
    t.text     "dyn_stylesheet"
    t.string   "dyn_alt"
    t.string   "dyn_src"
    t.string   "dyn_value"
    t.string   "dyn_href"
    t.string   "dyn_target"
  end

  add_index "dyn_machine_dyn_elements", ["parent_id", "parent_type"], name: "index_dyn_machine_dyn_elements_on_parent_id_and_parent_type", using: :btree

  create_table "dyn_machine_dyn_navs", force: true do |t|
    t.string   "dyn_tag"
    t.string   "dyn_type"
    t.string   "dyn_class"
    t.string   "dyn_id"
    t.string   "dyn_style"
    t.string   "dyn_content"
    t.integer  "dyn_theme_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "dyn_stylesheet"
  end

  add_index "dyn_machine_dyn_navs", ["dyn_theme_id"], name: "index_dyn_machine_dyn_navs_on_dyn_theme_id", using: :btree

  create_table "dyn_machine_dyn_pages", force: true do |t|
    t.integer  "order"
    t.string   "theme"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dyn_content"
    t.string   "dyn_tag"
    t.string   "dyn_class"
    t.string   "dyn_type"
    t.string   "dyn_id"
    t.string   "dyn_style"
    t.integer  "dyn_theme_id"
    t.boolean  "show_nav",       default: true
    t.text     "dyn_stylesheet"
    t.string   "dyn_alt"
    t.string   "dyn_src"
    t.string   "dyn_value"
    t.string   "dyn_href"
    t.string   "dyn_target"
  end

  add_index "dyn_machine_dyn_pages", ["dyn_theme_id"], name: "index_dyn_machine_dyn_pages_on_dyn_theme_id", using: :btree

  create_table "dyn_machine_dyn_sections", force: true do |t|
    t.integer  "order"
    t.integer  "dyn_page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dyn_content"
    t.string   "dyn_tag"
    t.string   "dyn_class"
    t.string   "dyn_type"
    t.string   "dyn_id"
    t.string   "dyn_style"
    t.text     "dyn_stylesheet"
    t.string   "dyn_alt"
    t.string   "dyn_src"
    t.string   "dyn_value"
    t.string   "dyn_href"
    t.string   "dyn_target"
  end

  add_index "dyn_machine_dyn_sections", ["dyn_page_id"], name: "index_dyn_machine_dyn_sections_on_dyn_page_id", using: :btree

  create_table "dyn_machine_dyn_themes", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "web_magic_pages", force: true do |t|
    t.string   "theme"
    t.integer  "num"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "web_magic_sections", force: true do |t|
    t.integer  "page_id"
    t.integer  "num"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "web_magic_sections", ["page_id"], name: "index_web_magic_sections_on_page_id", using: :btree

  create_table "web_magic_templates", force: true do |t|
    t.integer  "section_id"
    t.text     "content",    default: [], array: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "web_magic_templates", ["section_id"], name: "index_web_magic_templates_on_section_id", using: :btree

end
