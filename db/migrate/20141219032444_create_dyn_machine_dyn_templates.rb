class CreateDynMachineDynTemplates < ActiveRecord::Migration
  def change
    create_table :dyn_machine_dyn_templates do |t|
      t.integer :order
      t.string :content, array: true, default: []
      t.references :dyn_section, index: true

      t.timestamps
    end
  end
end
