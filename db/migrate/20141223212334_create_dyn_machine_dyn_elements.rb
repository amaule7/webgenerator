class CreateDynMachineDynElements < ActiveRecord::Migration
  def change
    create_table :dyn_machine_dyn_elements do |t|

      t.string :dyn_tag
      t.string :dyn_class
      t.string :dyn_type
      t.string :dyn_id
      t.string :dyn_style
      t.references :parent, polymorphic: true, index: true, name: "my index"
      t.timestamps
    end
  end
end
