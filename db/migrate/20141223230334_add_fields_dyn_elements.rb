class AddFieldsDynElements < ActiveRecord::Migration
  def change
    add_column :dyn_machine_dyn_elements, :dyn_content, :string

    add_column :dyn_machine_dyn_pages, :dyn_content, :string
    add_column :dyn_machine_dyn_pages, :dyn_tag, :string
    add_column :dyn_machine_dyn_pages, :dyn_class, :string
    add_column :dyn_machine_dyn_pages, :dyn_type, :string
    add_column :dyn_machine_dyn_pages, :dyn_id, :string

    add_column :dyn_machine_dyn_sections, :dyn_content, :string
    add_column :dyn_machine_dyn_sections, :dyn_tag, :string
    add_column :dyn_machine_dyn_sections, :dyn_class, :string
    add_column :dyn_machine_dyn_sections, :dyn_type, :string
    add_column :dyn_machine_dyn_sections, :dyn_id, :string

    remove_column :dyn_machine_dyn_sections, :class_info
    remove_column :dyn_machine_dyn_pages, :class_info
  end
end
