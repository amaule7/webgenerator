class AddReferencesDynPage < ActiveRecord::Migration
  def change
    add_reference :dyn_machine_dyn_pages, :dyn_theme, index: true
    add_column :dyn_machine_dyn_pages, :show_nav, :boolean, default: true
  end
end
