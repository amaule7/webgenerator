class CreateDynMachineDynSections < ActiveRecord::Migration
  def change
    create_table :dyn_machine_dyn_sections do |t|
      t.integer :order
      t.string :class_info
      t.references :dyn_page, index: true

      t.timestamps
    end
  end
end
