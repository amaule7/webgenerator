class CreateDynMachineDynThemes < ActiveRecord::Migration
  def change
    create_table :dyn_machine_dyn_themes do |t|
      t.string :name

      t.timestamps
    end
  end
end
