class AddStylesheet < ActiveRecord::Migration
  def change
    add_column :dyn_machine_dyn_pages, :dyn_stylesheet, :text
    add_column :dyn_machine_dyn_sections, :dyn_stylesheet, :text
    add_column :dyn_machine_dyn_elements, :dyn_stylesheet, :text
  end
end
