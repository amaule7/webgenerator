class AddFields < ActiveRecord::Migration
  def change
    add_column :dyn_machine_dyn_sections, :dyn_style, :string
    add_column :dyn_machine_dyn_pages, :dyn_style, :string
  end
end
