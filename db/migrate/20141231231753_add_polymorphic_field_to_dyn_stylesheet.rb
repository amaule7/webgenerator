class AddPolymorphicFieldToDynStylesheet < ActiveRecord::Migration
  def change
    change_table :dyn_machine_dyn_stylesheets do |t|
      t.references :element, :polymorphic => true
    end
  end
end
