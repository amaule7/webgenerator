class CreateDynMachineDynPages < ActiveRecord::Migration
  def change
    create_table :dyn_machine_dyn_pages do |t|
      t.integer :order
      t.string :theme
      t.string :class_info

      t.timestamps
    end
  end
end
