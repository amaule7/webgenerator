require_dependency "dyn_machine/application_controller"
require 'dyn_machine/stylesheet_generator.rb'

module DynMachine
  class DynStylesheetsController < ApplicationController
    caches_page :show

    def show
      @section    = DynMachine::DynSection.find(params[:id])
      @stylesheet = StylesheetGenerator.new(@section).make_stylesheet
      respond_to do |format|
        # format.html {render text: @stylesheet.to_s}
        format.css  {render :text => @stylesheet, :content_type => "text/css"}
      end
    end

  end
end
