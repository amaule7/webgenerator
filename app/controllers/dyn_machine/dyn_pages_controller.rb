require_dependency "dyn_machine/page_renderer"

module DynMachine
  class DynPagesController < ApplicationController
    include DynMachine::PageRenderer

    def index
      @render_this = []

      # Create Stylesheet
      @section_ids = [DynMachine::DynSection.last.id]
      @section_ids.each{|s| @render_this << "<link href=/dyn_machine/dyn_stylesheets/#{s}.css rel=stylesheet type=text/css />"}

      # Create View
      @section_ids.each{|s| @render_this << dyn_translate(DynMachine::DynSection.find(s))}

      render html: @render_this.join.html_safe, layout: false

      # template = Sass::Rails::SassTemplate.new

      # rendering html and css from a controller {
      # @rend = []
      # @rend << '<link href="/dyn_machine/dyn_stylesheets/1.css" rel="stylesheet" type="text/css" />'
      # @rend << content_tag("div", nil, class: "div1")
      # @rend << content_tag("div", nil, class: "div2")
      # @rend = @rend.join.html_safe
      # render html: @rend
      # }

      # render :index, layout: "dyn_machine/application" #render dynamic css/html from a view
    end

  end

end
