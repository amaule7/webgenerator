module DynMachine
  class DynPage < ActiveRecord::Base
    belongs_to :dyn_themes
    has_many :dyn_sections

    def dyn_elements
      dyn_sections
    end
  end
end
