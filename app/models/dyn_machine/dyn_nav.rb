module DynMachine
  class DynNav < ActiveRecord::Base
    belongs_to :dyn_theme
    has_many :dyn_elements, as: :parent
  end
end
