module DynMachine
  class DynTheme < ActiveRecord::Base
    has_many :dyn_pages
    has_one :dyn_nav
  end
end
