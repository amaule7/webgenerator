module DynMachine
  class DynSection < ActiveRecord::Base
    belongs_to :dyn_page
    has_many :dyn_elements, as: :parent

    # validates :dyn_stylesheet, presence: true

  end
end
