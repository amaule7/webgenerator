module DynMachine
  class DynElement < ActiveRecord::Base
    belongs_to :dyn_section, polymorphic: true
    belongs_to :dyn_nav, polymorphic: true
    belongs_to :element_parent, class_name: "Dyn_Machine::DynElement", polymorphic: true
    has_many :dyn_elements, source: "DynMachine::DynElement", as: :parent
  end
end
